<?php
$vars = [
    'phone' => '(385) 482-0007',
    'email' => 'steve@stephengashler.com',
    'rates' => [
        'thousand' => '$50 per thousand words',
        'word_credited' => '$.30 per word',
        'word_ghostwriter' => '$.40 per word',
        'hour' => '$50 per hour'
    ]
];
