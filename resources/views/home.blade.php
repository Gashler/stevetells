@extends('layouts.default')
@section('header_scripts')
    <script src="/js/controllers/HomeController.js"></script>
@stop
@section('content')
    <div ng-controller="HomeController">
        <div id="hero">
            <section>
                <div class="content">
                    <div>
                        <br class="show-lg">
                        <br class="show-lg">
                        <h3>National Award-winning Entertainer</h3>
                        <h1>Stephen Gashler</h1>
                        <ul id="types">
                            <li>Storyteller</li>
                            <li>Comedian</li>
                            <li>Musician</li>
                            <li>Motivational Speaker</li>
                        </ul>
                        <img class="show-lg" src="/img/home/hero.jpg" alt="Storyteller Stephen Gashler">
                        <br>
                        <br>
                        <a href="#contact" class="button white">Book a Performance</a>
                    </div>
                </div>
            </section>
            <section id="awards">
                <?php
                    $path = '/img/home/awards/';
                    $files = scandir(public_path() . $path);
                    foreach ($files as $file) {
                        if ($file !== '.' && $file !== '..') {
                ?>
                    <img src="{{ $path . $file }}">
                <?php } } ?>
                <br class="show-lg">
                <br class="show-lg">
                <br class="show-lg">
            </section>
        </div>
        <section id="form.quotes">
            <div class="row">
                <div class="col-6">
                    <table class="grid">
                        <tr>
                            <td style="background: url(/img/portfolio/lg/stephen-gashler-telling-school.jpg); width: 66.6%" colspan="2"></td>
                            <td style="background: url(/img/portfolio/lg/IMG_5804.jpg); background-position: 50% 15%;"></td>
                        </tr>
                        <tr>
                            <td style="background: url(/img/portfolio/lg/ext.jpg); width: 66.6%; background-position: 50%;" colspan="2"></td>
                            <td style="background: url(/img/portfolio/lg/960201_565384493541860_121901375_n.jpg); width: 33.3%; background-position: 0 15%"></td>
                        </tr>
                        <tr>
                            <td style="background: url(/img/portfolio/lg/1012937_785189034894737_8311434653148315111_n.jpg); width: 33.3%; background-position: 50%;"></td>
                            <td style="background: url(/img/portfolio/lg/IMG_3590-e1441595136554.jpg); width: 33.3%; background-position: 50%;" rowspan="2"></td>
                            <td style="background: url(/img/portfolio/lg/stephen-gashler-hauntings.jpg); width: 33.3%; background-position: 45%;" rowspan="2"></td>
                        </tr>
                        <tr>
                            <td style="background: url(/img/portfolio/lg/stephen-gashler-cello.jpg)"></td>
                        </tr>
                    </table>
                </div>
                <div class="col-6" style="padding: 5vw 5%;">
                    <p class="quote"><span class="quote-symbol">"</span><span>Stephen Gashler has a gift for capturing the imaginations of all ages."</span></p>
                    <cite>Richard Thurman<br>Director, Utah Renaissance Faire</cite>
                    <p class="quote"><span class="quote-symbol">"</span>Stephen Gashler has excellent comedic timing"</span></p>
                    <cite>Front Row Reviewers Utah</cite>
                    <p class="quote"><span class="quote-symbol">"</span>Stephen Gashler shows all the promise of a truly great artist."</span></p>
                    <cite>Utah Theatre Bloggers</cite>
                    <p class="quote"><span class="quote-symbol">"</span><span>Stephen Gashler has a good sense of story. He has the ability to choose material that fits his audience. His performances are memorable and abound with energy. The twinkle in his eyes makes you wonder what’s coming next!"</span></p>
                    <cite>Carla Morris<br>Children’s Services Manager, Provo Library</cite>
                </div>
            </div>
        </section>
        <section id="ceo" class="inverted">
            <div>
                <p>
                    <img width="200" class="pull-left" src="/img/home/stephen-gashler.jpg">
                    <strong>Stephen Gashler has been entertaining children and adults for over ten years through storytelling, stand-up comedy, and music.</strong> He's been the featured performer at countless libraries, schools, festivals, concerts, and private events. His repetoire ranges from hilarious memoirs to folk tales, tall tales, ghost stories, motivational speaking, and true stories from history. If you're looking for a good laugh, a bit of magic, and a healthy dose of insipiration, you've come to the right guy.</p>
                    <p>Stephen Gashler is also a playwright, composer, and author for young readers. He offers workshops to help aspiring storytellers develop their art.</p>
                </p>
            </div>
            <div id="contact">
                <h2>
                    <span ng-show="!package">Book a Show / Ask a Question</span>
                    <span ng-show="package">Get Started</span>
                </h2>
                <div class="alert success" ng-if="package">
                    <strong>Your Package:</strong> @{{ package.name }} - $@{{ package.price }}
                    <ul>
                        <li class="fa fa-check" ng-repeat="service in services" ng-show="inArray($index, package.services)">@{{ service }}</li>
                    </ul>
                </div>
                <p>Fill out the form below or call <strong>{{ config('site.phone') }}</strong>. Typical performances are about an hour but can be shortened or lengthened depending on your needs.</p>
                <form ng-submit="submitForm()">
                    {{-- <div class="inline-block">
                        <label>Your Venue:</label>
                        <select ng-model="venue" ng-options="venue as venue.name for venue in venues"></select>
                    </div> --}}
                    <div class="inline-block">
                        <label>Organization Type:</label>
                        <select ng-model="form.organization_type" ng-change="updateQuote()" ng-options="organization_type as organization_type.name for organization_type in organization_types"></select>
                    </div>
                    <div class="inline-block">
                        <label>Estimated Attendees:</label>
                        <select ng-model="form.attendees" ng-change="updateQuote()" ng-options="attendees as attendees.name for attendees in estimated_attendees"></select>
                    </div>
                    <div class="inline-block">
                        <label>Back-to-Back Performances:</label>
                        <select ng-model="form.performances" ng-change="updateQuote()" ng-options="performances for performances in number_of_performances"></select>
                    </div>
                    <div class="inline-block">
                        <label>Approximate Performance Location:</label>
                        <input ng-model="form.location" ng-blur="getDistance()" type="text" placeholder="zip code or city + state">
                    </div>
                    <div class="alert success" ng-show="form.quote > 5">
                        <div ng-show="form.discount">
                            <strong>Your Discount:</strong><br>
                            @{{ form.discount | currency }}
                            <br>
                            <br>
                        </div>
                        <strong>Your Quote</strong><br>
                        <div ng-show="form.discount">
                            <div class="strike">
                                @{{ form.original_quote_gross | currency }}
                                <div class="line"></div>
                            </div>
                        </div>
                        @{{ form.quote | currency }}
                    </div>
                    <br>
                    <input ng-model="form.Name" type="text" placeholder="Your Name">
                    <input ng-model="form.Company" type="text" placeholder="Your Organization (optional)">
                    <input ng-model="form.Email" type="text" placeholder="Your Email Address">
                    <input ng-model="form.Phone" type="text" placeholder="Your Phone Number (optional)">
                    <textarea ng-model="form.Comments" rows="10" placeholder="Please provide a little information about your event and what you're looking for."></textarea>
                    <button ng-show="!submitting && !submitted" class="button">
                        <span ng-show="!package">Submit Request</span>
                        <span ng-show="package">Get Started</span>
                    </button>
                    <img class="loading" ng-show="submitting" src="/img/loading.gif">
                </form>
            </div>
        </section>
        <section id="services">
            <div id="technologies" style="width: 17%">
                <h2>Fantastic</h2>
                <ul>
                    <li>Tall Tales</li>
                    <li>Holiday Stories</li>
                    <li>Ghost Stories</li>
                    <li>Celtic Fairy Tales</li>
                    <li>Norse and Greek Mythology</li>
                    <li>Folk Tales From Around the World</li>
                    <li>Arthurian Legends</li>
                </ul>
            </div>
            <div id="platforms" style="width: 17%">
                <h2>Memoirs</h2>
                <ul>
                    <li>Stand-up Comedy</li>
                    <li>Turning Off the TV and Defeating the Dark Lord of Boredom</li>
                    <li>Coming of Age (for young audiences)</li>
                    <li>Anti-bullying</li>
                </ul>
            </div>
            <div id="services-list" style="width: 17%">
                <h2>Historical</h2>
                <ul>
                    <li>Pioneer Stories</li>
                    <li>War Stories</li>
                    <li>Heroes from American History</li>
                    <li>Biblical Stories</li>
                </ul>
            </div>
            <div id="systems" style="width: 50%">
                <div class="videoWrapper">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/zIgRfQRpWYc?rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="popup" id="modulePopup" ng-click="togglePopup('#modulePopup')">
                <div class="panel">
                    <h2 class="@{{ module.class }}"><i class="fa fa-@{{ module.icon }}"></i> @{{ module.name }}</h2>
                    <p>@{{ module.description }}</p>
                    <button class="button black" ng-click="togglePopup('#modulePopup')">Close</button>
                </div>
            </div>
            <div class="popup" id="imgPopup" ng-click="togglePopup('#imgPopup')">
                <div class="panel align-center">
                    <img src="@{{ img }}">
                    <br>
                    <br>
                    <button class="button black" ng-click="togglePopup('#imgPopup')">Close</button>
                </div>
            </div>
        </section>
        {{-- <div class="popup" id="requestSent" ng-click="togglePopup('#requestSent')">
            <div class="panel">
                <h2 class="@{{ module.class }}">Thank You For Your Request!</h2>
                <p>You'll be contacted within one business with more information.</p>
                <button class="button black" ng-click="togglePopup('#requestSent')">Close</button>
            </div>
        </div> --}}
    </div>
@stop
