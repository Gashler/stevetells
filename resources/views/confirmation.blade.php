@extends('layouts.default')
@section('content')
    <style>header { display: none; }</style>
    <section style="padding: 5vw 5%">
        <div class="alert success">
            Thank you for your request! You'll be contacted within one business day with more information.
        </div>
        <br>
        <a class="button" href="/"><i class="fa fa-angle-left"></i> Back</a>
    </section>
@stop
@section('footer_scripts')
    <!-- Google Code for Send Contact Request Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 834721556;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "A6aXCKOHkHUQlK6DjgM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/834721556/?label=A6aXCKOHkHUQlK6DjgM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
@stop
