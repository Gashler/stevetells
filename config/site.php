<?php

return [
    'title' => 'Stephen Gashler | Storyteller, Comedian, Musican, Motivational Speaker',
    'company_name' => 'Stephen Gashler',
    'phone' => '(385) 482-0007',
    'email' => 'steve@stephengashler.com',
];
