var app = angular.module('app', [])
.controller('HomeController', function($scope, $http) {

    // define variables
    $scope.submitting = false;
    $scope.submitted = false;
    $scope.form = {};
    $scope.animating = false;
    $scope.form.miles = 1;
    $scope.form.quote = 0;
    $scope.performance_rate = 300;
    $scope.miles_rate = .5;
    $scope.form.discount = 0;

    $scope.services = [
        "Build Your Templated Wordpress Website",
        "Up to 5 pages, blog, contact form",
        "Deploy Your Website on a Server",
        "Set up account with payment processer",
        "Integrate WooCommerce system",
        "Integrate Google Analytics & Other Third Party Services",
        "Complete Custom Theme for Your Business",
        "10 additional hours of custom development"
    ];

    // $scope.venues = [
    //     {
    //         name: 'Library',
    //         price: '150'
    //     },
    //     {
    //         name: 'School',
    //         price: '300'
    //     },
    //     {
    //         name: 'Public Event',
    //         price: '300'
    //     },
    //     {
    //         name: 'Private Event',
    //         price: '150'
    //     }
    // ];
    // $scope.venue = $scope.venues[0];

    $scope.organization_types = [
        {
            name: 'Select One',
            rate: 1
        },
        {
            name: 'For Profit',
            rate: 1
        },
        {
            name: 'Non-profit',
            rate: .5
        }
    ];
    $scope.form.organization_type = $scope.organization_types[0];

    $scope.estimated_attendees = [
        {
            name: 'Select One',
            rate: 0
        },
        {
            name: '< 100',
            rate: 1
        },
        {
            name: '100 - 500',
            rate: 2
        },
        {
            name: '500 - 1000',
            rate: 3
        },
        {
            name: '> 1000',
            rate: 4
        }
    ];
    $scope.form.attendees = $scope.estimated_attendees[0];

    $scope.number_of_performances = [1,2,3,4,5,6];
    $scope.form.performances = $scope.number_of_performances[0];

    $scope.updateQuote = function()
    {
        if ($scope.form.miles < 1) {
            $scope.form.miles = 0;
        }
        $scope.miles = $scope.miles_rate * $scope.form.miles;
        $scope.updated_rate = $scope.performance_rate * $scope.form.attendees.rate;
        $scope.form.quote = $scope.updated_rate * $scope.form.performances;
        $scope.original_quote_net = $scope.form.quote * $scope.organization_types[0].rate;
        $scope.form.original_quote_gross = $scope.original_quote_net + $scope.miles;
        if ($scope.form.organization_type.name == 'Non-profit') {
            $scope.updated_rate *= $scope.form.organization_type.rate;
            $scope.form.quote = $scope.updated_rate * $scope.form.performances;
        }
        $scope.form.discount = $scope.original_quote_net - $scope.form.quote;
        $scope.form.quote += $scope.miles;
        if (extra_performances = $scope.form.performances - 1) {
            $scope.form.discount += ($scope.updated_rate * extra_performances) / 2;
            $scope.form.quote = $scope.form.original_quote_gross - $scope.form.discount;
        }
    }
    $scope.updateQuote();

    $scope.submitForm = function()
    {
        $scope.form.attendees = $scope.form.attendees.name;
        $scope.form.organization_type = $scope.form.organization_type.name;
        if (
            !$scope.form.Name
            || !$scope.form.Email
        ) {
            return alert("Please provide your name and email address.");
        }
        $scope.submitting = true;
        $http.post('/send-message', $scope.form).then(function(response) {
            window.location = '/message-sent';
        });
    }

    $scope.inArray = function(needle, haystack)
    {
        var length = haystack.length;
        for(var i = 0; i < length; i++) {
            if(haystack[i] == needle) return true;
        }
        return false;
    }

    $scope.togglePopup = function(popup)
    {
        if (!$scope.animating) {
            $scope.animating = true;
            $(popup).fadeToggle(function() {
                $scope.animating = false;
            });
        }
    }

    $scope.getDistance = function()
    {

        var origin1 = '84606';
        var destinationA = $scope.form.location;

        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
          {
            origins: [origin1],
            destinations: [destinationA],
            travelMode: 'DRIVING',
            // transitOptions: TransitOptions,
            // drivingOptions: DrivingOptions,
            unitSystem: google.maps.UnitSystem.IMPERIAL,
            // avoidHighways: Boolean,
            // avoidTolls: Boolean,
          }, callback);

        function callback(response, status) {
            $scope.form.location = response.destinationAddresses[0];
            $scope.form.miles = response.rows[0].elements[0].distance.text.split(" ");
            $scope.form.miles = $scope.form.miles[0];
            $scope.updateQuote();
            $scope.$apply();
        }
    }

});
